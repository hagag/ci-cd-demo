# How to register a runner
1. Go to where your Gitlab runner executable downloaded
1. Run the register command:
  ```
  ./gitlab-runner.exe register
  ```
1. Follow the instruction on the screen
  