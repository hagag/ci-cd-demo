var chai = require("chai");
var expect = chai.expect;

var Adder = require("../helper/adder.js");
var adder = new Adder();

describe("Test suit ", function() {
  it("should return sum of the numbers", function() {
    expect(adder.add(1, 2)).to.equal(3);
  });
  
});
